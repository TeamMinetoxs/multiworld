<?php

namespace MultiWorld;

use pocketmine\math\Vector3;
use pocketmine\utils\TextFormat as MT;
use pocketmine\command\CommandSender;
use pocketmine\plugin\PluginBase;
use pocketmine\command\Command;
use pocketmine\utils\Config;
use pocketmine\Server;
use pocketmine\item\Item;
use pocketmine\level\Level;
use pocketmine\Player;

class main extends PluginBase
{

    public function onEnable()
    {

    }

    /**
     * @param CommandSender $sender
     * @param Command $cmd
     * @param string $label
     * @param array $args
     * @return void
     */
    public function onCommand(CommandSender $sender, Command $cmd, $label, array $args)
    {
        switch($cmd->getName())
        {
            case 'multiworld':
                if(isset($args[0]))
                {
                    switch($args[0])
                    {
                        case "load":
                            if(isset($args[1]))
                            {
                                if($this->getServer()->isLevelLoaded($args[1]))
                                {
                                    $sender->sendMessage("[MultiWorld] Level is already loaded");
                                }
                                else
                                {
                                    if($sender instanceof Player) //Duo duplicated Console Messages from Pocketmine-MP
                                    {
                                        if($this->getServer()->loadLevel($args[1]))
                                        {
                                            $sender->sendMessage("[MultiWorld] Loading Level \"" . $args[1] . "\"");
                                        }
                                        else
                                        {
                                            $sender->sendMessage("[MultiWorld] Level \"" . $args[1] . "\" not found");
                                        }
                                    }
                                    else
                                    {
                                        $this->getServer()->loadLevel($args[1]);
                                    }
                                }
                            }
                            else
                            {
                                $sender->sendMessage("[MultiWorld] Command Usage: /mw load <world>");
                            }
                            break;
                        case "goto":
                            if($sender instanceof Player)
                            {
                                if(isset($args[1]))
                                {
                                    if($this->getServer()->isLevelLoaded($args[1]))
                                    {
                                        $sender->teleport($this->getServer()->getLevelbyName($args[1])->getSpawnLocation());
                                    }
                                    else
                                    {
                                        $sender->sendMessage("[MultiWorld] Level \"" . $args[1] . "\" not loaded");
                                    }
                                }
                                else
                                {
                                    $sender->sendMessage("[Multiworld] Command Usage: /mw goto <world>");
                                }
                            }
                            else
                            {
                                $sender->sendMessage("[MultiWorld] Please use this Command in-game");
                            }
                            break;
                        case "unload":
                            if(isset($args[1]))
                            {
                                if($sender instanceof Player)
                                {
                                    if ($this->getServer()->isLevelLoaded($args[1]))
                                    {
                                        $this->getServer()->getLevelByName($args[1])->unload();
                                        $sender->sendMessage("[MultiWorld] Unloading Level \"" . $args[1] . "\"");
                                    }
                                    else
                                    {
                                        $sender->sendMessage("Level is not loaded");
                                    }
                                }
                                else
                                {
                                    $this->getServer()->getLevelByName($args[1])->unload();
                                }
                            }
                            else
                            {
                                $sender->sendMessage("[Multiworld] Command Usage: /mw unload <world>");
                            }
                            break;
                        case "spawn":
                            if($sender instanceof Player)
                            {
                                $sender->teleport($sender->getLevel()->getSafeSpawn());
                            }
                            else
                            {
                                $sender->sendMessage("[MultiWorld] Please use this Command in-game");
                            }
                            break;
                        case "setspawn":
                            if($sender instanceof Player)
                            {
                                $level = $sender->getLevel();
                                $level->setSpawnLocation($sender->getPosition());
                            }
                            else
                            {
                                $sender->sendMessage("[MultiWorld] Please use this Command in-game");
                            }
                            break;
                        case "info": //TODO
                            if($sender instanceof Player)
                            {
                                $x = round($sender->getX());
                                $y = round($sender->getX());
                                $z = round($sender->getZ());
                                $level = $sender->getLevel()->getName();
                                $sender->sendMessage("[MultiWorld] You are at X: ". $x . " Y:" . $y ." Z: " . $z . " on World: \"".$level."\"");
                            }
                            else
                            {

                            }
                            break;
                        case "create": //TODO
                            if(isset($args[1]))
                            {
                                if($this->getServer()->generateLevel($args[1]))
                                {
                                    if($sender instanceof Player) //Duo Duplicated Console Message from Pocketmine-MP
                                    {
                                        $sender->sendMessage("Generating World \"" . $args[1] . "\"");
                                    }
                                }
                                else
                                {
                                    $sender->sendMessage("[MultiWorld]Level \"" . $args[1] . "\" already exits");
                                }
                            }
                            else
                            {
                                $sender->sendMessage("[MutliWorld] Command Usage: /mw create <name>");
                            }
                            break;
                        case "help":
                        default:
                            $sender->sendMessage("===============================");
                            $sender->sendMessage(">> /mw load <world>");
                            $sender->sendMessage(">> /mw goto <world>");
                            $sender->sendMessage(">> /mw unload <world>");
                            $sender->sendMessage("===============================");
                            break;
                    }
                }
                else
                {
                    $sender->sendMessage("===============================");
                    $sender->sendMessage(">> /mw load <world>");
                    $sender->sendMessage(">> /mw goto <world>");
                    $sender->sendMessage(">> /mw unload <world>");
                    $sender->sendMessage("===============================");
                }
                break;
        }
    }

    public function onDisable()
    {

    }
}